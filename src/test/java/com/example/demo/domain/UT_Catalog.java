/*
 * Algebra labs.
 */

package com.example.demo.domain;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.example.demo.config.SpringConfig;
import com.example.demo.service.Catalog;

public class UT_Catalog {

	@Test
	public void catalogTest() {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);

		Catalog cat = ctx.getBean(Catalog.class);
		
		System.out.println("\n*** Retrieving item from the database ***");
		System.out.println(cat.findById(1L));
		System.out.println("***\n");

		ctx.close();
	}

}
